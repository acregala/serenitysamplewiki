package org.wikipedia.features.homepage;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.wikipedia.steps.UserSteps;

@RunWith(SerenityRunner.class)
public class TestCaseHomePage {
	@Managed
    public WebDriver webdriver;
	
	String expectedTitle = "Wikipedia, the free encyclopedia";
	
	String expectedArticleCount = "5,488,198 articles in English";
	
	@Steps
	UserSteps claude;
	

	
	@Test
	public void testCase1() throws InterruptedException{
		claude.opens_home_page();
		claude.checks_home_page_title(expectedTitle);
		claude.checks_number_of_article_value(expectedArticleCount);
		claude.clicks_article_count();
	}
	
}
