package org.wikipedia.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WebElementState;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://en.wikipedia.org/wiki/Main_Page")
public class HomePage extends PageObject{

	//	WebElement articleCount = driver.findElement(By.id("articlecount"));
	@FindBy(id="articlecount")
	private WebElementFacade articleCountDiv;
	
	public void clickArticleCount(){
		articleCountDiv.waitUntilClickable().click();
	}
	
	public WebElementFacade articleCountDiv(){
		return articleCountDiv;
	}
}
