package org.wikipedia.steps;

import net.thucydides.core.annotations.Step;

import org.junit.Assert;
import org.wikipedia.pages.HomePage;

public class UserSteps {

	HomePage homePage;
	
	@Step
	public void opens_home_page(){
		homePage.open();
	}
	
	@Step
	public void clicks_article_count(){
		homePage.clickArticleCount();
	}
	
	@Step
	public void checks_number_of_article_value(String articleCount){
		Assert.assertEquals(articleCount,homePage.articleCountDiv().getTextValue());
	}
	
	@Step
	public void checks_home_page_title(String title){
		Assert.assertEquals(title,homePage.getTitle());
	}
}
