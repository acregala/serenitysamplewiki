package org.wikipedia.driver;

import io.github.bonigarcia.wdm.ChromeDriverManager;

import java.util.Arrays;
import java.util.HashMap;

import net.thucydides.core.webdriver.DriverSource;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ChromeCustomDriver implements DriverSource {
	public WebDriver newDriver() {
        try {
            ChromeDriverManager.getInstance().setup();
            return new ChromeDriver();
        } catch(Exception e) {
            throw new Error(e);
        }
    }

    public boolean takesScreenshots() {
        return false;
    }
}

