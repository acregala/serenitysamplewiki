package org.wikipedia.driver;

import io.github.bonigarcia.wdm.Architecture;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import net.thucydides.core.webdriver.DriverSource;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class InternetExplorerCustomDriver implements DriverSource {
	public WebDriver newDriver() {
        try {
        	InternetExplorerDriverManager.getInstance().setup();
			DesiredCapabilities ieCapabilities = DesiredCapabilities
					.internetExplorer();
			ieCapabilities
					.setCapability(
							InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
            return new InternetExplorerDriver();
        } catch(Exception e) {
            throw new Error(e);
        }
    }

    public boolean takesScreenshots() {
        return false;
    }
}

