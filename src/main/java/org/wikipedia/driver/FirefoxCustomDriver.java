package org.wikipedia.driver;

import io.github.bonigarcia.wdm.Architecture;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import net.thucydides.core.webdriver.DriverSource;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxCustomDriver implements DriverSource {
	public WebDriver newDriver() {
        try {
        	FirefoxDriverManager.getInstance().architecture(Architecture.x32).version(".18").setup();
            return new FirefoxDriver();
        } catch(Exception e) {
            throw new Error(e);
        }
    }

    public boolean takesScreenshots() {
        return false;
    }
}

